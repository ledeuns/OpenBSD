pkg_add git cmake go libevent llvm boehm-gc libiconv gmake pcre

git clone https://boringssl.googlesource.com/boringssl

cd boringssl

git checkout bfe527fa35735e8e045cbfb42b012e13ca68f9cf

BORINGSSL=$PWD

git apply ../boringssl.diff

cmake -DCMAKE_BUILD_TYPE=Release . && make

cd ..

git clone https://github.com/litespeedtech/lsquic.git

cd lsquic

git submodule init

git submodule update

git apply ../lsquic.diff

cmake -DBORINGSSL_DIR=$BORINGSSL .

make

cd ..

git clone https://github.com/omarroth/invidious

cd invidious

shards update && shards install

cp ../boringssl/ssl/libssl.a lib/lsquic/src/lsquic/ext/

cp ../boringssl/crypto/libcrypto.a lib/lsquic/src/lsquic/ext/

cp ../lsquic/src/liblsquic/liblsquic.a lib/lsquic/src/lsquic/ext/

crystal build src/invidious.cr